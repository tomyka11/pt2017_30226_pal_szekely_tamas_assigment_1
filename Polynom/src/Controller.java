import java.awt.event.*;

public class Controller {
	//atributele clase cotroller sunt clase declarate anterior
	private Monom mon;
	private Polynom pol;
	private Polynom pol2;
	private Operations op;
	private Visualisare vis;
	
	public Controller(Monom mon1,Polynom pol1,Polynom pol3,Operations op1,Visualisare vis1){
		mon=mon1;
		pol=pol1;
		pol2=pol3;
		op=op1;
		vis=vis1;
		//prin constructor se face initializarea
		vis.addMonom1Listener(new Monom1Listener());
		vis.addMonom2Listener(new Monom2Listener());
		vis.addDerivate1Listener(new Derivate1Listener());
		vis.addDerivate2Listener(new Derivate2Listener());
		vis.addIntegrate1Listener(new Integrate1Listener());
		vis.addIntegrate2Listener(new Integrate2Listener());
		vis.addAddListener(new AddListener());
		vis.addSubstractListener(new SubstractListener());
		vis.addMultiplyListener(new MultiplyListener());
		vis.addInitializateListener(new InitializateListener());
		
		
	}
	
//aici se face implementarii actionListeneriilor ,fiecarui buton

class Monom1Listener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
		Monom mon=new Monom();
		mon.setCoefficient(Integer.parseInt(vis.getfirstcoef()));
		mon.setGrade(Integer.parseInt(vis.getfirstgrad()));
		pol.addMonom(mon);
		vis.setfirstpol(pol.wrpol());
		
	}
	
	
	
}

class Monom2Listener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
		Monom mon=new Monom();
		mon.setCoefficient(Integer.parseInt(vis.getsecondcoef()));
		mon.setGrade(Integer.parseInt(vis.getsecondgrad()));
		pol2.addMonom(mon);
		vis.setsecondpol(pol2.wrpol());
		
	}
	
	
	
}

class Derivate1Listener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
	  Polynom polder=op.derivatePolynom(pol);
	  vis.setderivate1(polder.wrpol());
	  
		
	}
	
	
	
}

class Derivate2Listener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
	  Polynom polder=op.derivatePolynom(pol2);
	  vis.setderivate2(polder.wrpol());
	  
		
	}
	
	
	
}


class Integrate1Listener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
	  Polynom polint=op.integratePolynom(pol);
	  vis.setintegrate1(polint.wrpol());
	  
		
	}
	
	
	
}


class Integrate2Listener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
	  Polynom polint=op.integratePolynom(pol2);
	  vis.setintegrate2(polint.wrpol());
	  
		
	}
	
	
	
}

class AddListener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
	  Polynom polint=op.addPolynoms(pol,pol2);
	  vis.setaddproduct(polint.wrpol());
	  
		
	}
	
	
	
}


class SubstractListener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
	  Polynom polint=op.subPolynoms(pol,pol2);
	  vis.setsubproduct(polint.wrpol());
	  
		
	}
	
	
	
}

class MultiplyListener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
	  Polynom polint=op.multiplyPolynoms(pol,pol2);
	  vis.setmultiplyproduct(polint.wrpol());
	  
		
	}
	
	
	
}

class InitializateListener implements ActionListener{
	public void actionPerformed(ActionEvent e)
	{
		vis.coef_gui1.setText("0");
		vis.grad_gui1.setText("0");
		vis.coef_gui2.setText("0");
		vis.grad_gui2.setText("0");
		vis.first_polynom.setText("0");
		vis.first_polynom.setEditable(false);
		vis.second_polynom.setText("0");
		vis.second_polynom.setEditable(false);
		vis.derivate1.setText("0");
		vis.derivate1.setEditable(false);
		vis.derivate2.setText("0");
		vis.derivate2.setEditable(false);
		vis.integrate1.setText("0");
		vis.integrate1.setEditable(false);
		vis.integrate2.setText("0");
		vis.integrate2.setEditable(false);
		vis.add_product.setText("0");
		vis.add_product.setEditable(false);
		vis.sub_product.setText("0");
		vis.sub_product.setEditable(false);
		vis.multiply_product.setText("0");
		vis.multiply_product.setEditable(false);
		pol=new Polynom();
		pol2=new Polynom();
	  
		
	}
	
	
	
}


}
