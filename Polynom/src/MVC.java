
import javax.swing.*;

public class MVC {
	public static void main(String [] args){
		Monom mon=new Monom();
		Polynom pol=new Polynom();
		Polynom pol2=new Polynom();
		Operations op=new Operations();
		
		Visualisare visualisare=new Visualisare(mon,pol,op);
		Controller cont=new Controller(mon,pol,pol2,op,visualisare);
		visualisare.setVisible(true);
	}

}