import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Visualisare extends JFrame {
	//declaram componentele GUI
	public JTextField coef_gui1=new JTextField(5);
	public JTextField grad_gui1=new JTextField(5);
	public JButton add_monom1=new JButton("Add fo First Polynom");
	public JTextField coef_gui2=new JTextField(5);
	public JTextField grad_gui2=new JTextField(5);
	public JButton add_monom2=new JButton("Add fo Second Polynom");
	public JTextField first_polynom=new JTextField(20);
	public JTextField second_polynom=new JTextField(20);
	public JTextField derivate1=new JTextField(20);
	public JTextField integrate1=new JTextField(20);
	public JTextField derivate2=new JTextField(20);
	public JTextField integrate2=new JTextField(20);
	public JButton do_deriv1=new JButton("Derivate First Polynom");
	public JButton do_integrate1=new JButton("Integrate First Polynom");
	public JButton do_deriv2=new JButton("Derivate Second Polynom");
	public JButton do_integrate2=new JButton("Integrate Second Polynom");
	public JTextField add_product=new JTextField(20);
	public JTextField sub_product=new JTextField(20);
	public JTextField multiply_product=new JTextField(20);
	public JButton addb=new JButton("Add");
	public JButton substractb=new JButton("Substract");
	public JButton multiplyb=new JButton("Multiply");
	public JButton initializate=new JButton("Initializate");
	
	private Monom mon;
	private Polynom pol;
	private Operations op;
	
	
	public Visualisare(Monom monom,Polynom polynom,Operations operations)
	{
		mon=monom;
		pol=polynom;
		op=operations;
		//initializam componentele
		coef_gui1.setText("0");
		grad_gui1.setText("0");
		coef_gui2.setText("0");
		grad_gui2.setText("0");
		first_polynom.setText("0");
		first_polynom.setEditable(false);
		second_polynom.setText("0");
		second_polynom.setEditable(false);
		derivate1.setText("0");
		derivate1.setEditable(false);
		derivate2.setText("0");
		derivate2.setEditable(false);
		integrate1.setText("0");
		integrate1.setEditable(false);
		integrate2.setText("0");
		integrate2.setEditable(false);
		add_product.setText("0");
		add_product.setEditable(false);
		sub_product.setText("0");
		sub_product.setEditable(false);
		multiply_product.setText("0");
		multiply_product.setEditable(false);
		//se facem mai multe panouri pentru dimensionarea totala a spatiului
		JPanel phoriz=new JPanel();
		phoriz.setLayout(new BoxLayout(phoriz,BoxLayout.Y_AXIS));
		JPanel panel1=new JPanel();
		panel1.setLayout(new FlowLayout());
		JPanel panel2=new JPanel();
		panel2.setLayout(new FlowLayout());
		JPanel panel3=new JPanel();
		panel3.setLayout(new FlowLayout());
		JPanel panel4=new JPanel();
		panel4.setLayout(new FlowLayout());
		JPanel panel5=new JPanel();
		panel5.setLayout(new FlowLayout());
		JPanel panel6=new JPanel();
		panel6.setLayout(new FlowLayout());
		JPanel panel7=new JPanel();
		panel7.setLayout(new FlowLayout());
		JPanel panel8=new JPanel();
		panel8.setLayout(new FlowLayout());
		JPanel panel9=new JPanel();
		panel9.setLayout(new FlowLayout());
		JPanel panel10=new JPanel();
		panel10.setLayout(new FlowLayout());
		JPanel panel11=new JPanel();
		panel11.setLayout(new FlowLayout());
		JPanel panel12=new JPanel();
		panel12.setLayout(new FlowLayout());
		//adaugam componentele respective la fiecare panou
		phoriz.add(panel1);
		phoriz.add(panel2);
		phoriz.add(panel3);
		phoriz.add(panel4);
		phoriz.add(panel5);
		phoriz.add(panel6);
		phoriz.add(panel7);
		phoriz.add(panel8);
		phoriz.add(panel9);
		phoriz.add(panel10);
		phoriz.add(panel11);
		phoriz.add(panel12);
		panel1.add(new JLabel("Give a monom for first Poynom"));
		panel1.add(new JLabel("Give a monom for second Poynom"));
		panel2.add(new JLabel("Coef:"));
		panel2.add(new JLabel("Grad:"));
		panel2.add(new JLabel("Coef:"));
		panel2.add(new JLabel("Grad:"));
		panel3.add(coef_gui1);
		panel3.add(new JLabel("X^"));
		panel3.add(grad_gui1);
		panel3.add(coef_gui2);
		panel3.add(new JLabel("X^"));
		panel3.add(grad_gui2);
		panel4.add(add_monom1);
		panel4.add(add_monom2);
		panel5.add(new JLabel("First Polynom"));
		panel5.add(new JLabel("Second Polynom"));
		panel6.add(first_polynom);
		panel6.add(second_polynom);
	    panel7.add(do_deriv1);
	    panel7.add(do_integrate1);
	    panel7.add(do_deriv2);
	    panel7.add(do_integrate2);
	    panel8.add(derivate1);
	    panel8.add(integrate1);
	    panel8.add(derivate2);
	    panel8.add(integrate2);
	    panel9.add(new JLabel("Operations with Polynoms"));
	    panel10.add(addb);
	    panel10.add(substractb);
	    panel10.add(multiplyb);
	    panel11.add(add_product);
	    panel11.add(sub_product);
	    panel11.add(multiply_product);
	    panel12.add(initializate);
	    
	    this.setContentPane(phoriz);
	    this.pack();
	    this.setTitle("Polynoms");
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	//declaram metodele pentru setarea texturilor in GUI,iar si actionlistenerile
	public String getfirstcoef() {
        return coef_gui1.getText();
    }
	public String getsecondcoef() {
        return coef_gui2.getText();
    }
	public String getfirstgrad() {
        return grad_gui1.getText();
    }
	public String getsecondgrad() {
        return grad_gui2.getText();
    }
	public void setfirstpol(String newpol) {
        first_polynom.setText(newpol);
    }
	public void setsecondpol(String newpol) {
        second_polynom.setText(newpol);
    }
	public void setderivate1(String derivate){
		derivate1.setText(derivate);
	}
	public void setintegrate1(String integrate){
		integrate1.setText(integrate);
	}
	public void setderivate2(String derivate){
		derivate2.setText(derivate);
	}
	public void setintegrate2(String integrate){
		integrate2.setText(integrate);
	}
	public void setaddproduct(String product){
		add_product.setText(product);
	}
	public void setsubproduct(String product){
		sub_product.setText(product);
	}
	
	public void setmultiplyproduct(String product){
		multiply_product.setText(product);
	}
	
	public void addMonom1Listener(ActionListener mal) {
        add_monom1.addActionListener(mal);
    }
	public void addMonom2Listener(ActionListener mal) {
        add_monom2.addActionListener(mal);
    }
	public void addDerivate1Listener(ActionListener mal) {
        do_deriv1.addActionListener(mal);
    }
	public void addDerivate2Listener(ActionListener mal) {
        do_deriv2.addActionListener(mal);
    }
	public void addIntegrate1Listener(ActionListener mal) {
        do_integrate1.addActionListener(mal);
    }
	public void addIntegrate2Listener(ActionListener mal) {
        do_integrate2.addActionListener(mal);
    }
	public void addAddListener(ActionListener mal) {
        addb.addActionListener(mal);
    }
	public void addSubstractListener(ActionListener mal) {
        substractb.addActionListener(mal);
    }
	public void addMultiplyListener(ActionListener mal) {
        multiplyb.addActionListener(mal);
    }
	
	public void addInitializateListener(ActionListener mal){
		initializate.addActionListener(mal);
	}
	
	
	
	
	
	
	

}
