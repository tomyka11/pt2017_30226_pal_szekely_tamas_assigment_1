import java.util.*;

public class Polynom  {
//polinomul are ca atribut o lista de monoame
	public List<Monom> monomlyst =new ArrayList<Monom>();
	//definim si aici constructorul fara parametrii
	public Polynom(){
		super();
	}
	//construirea polinomului se face prin adaugarea mai multor monoam intr/o lista 
	public String addMonom(Monom exmonom){
		int t=0;
		for(Monom monom:monomlyst)
		{
			if (monom.getGrade()==exmonom.getGrade() ) //daca exista un monom cu acest grad in polinom ca monomul pe care  vrem sa-l adaugam,atunci calculam suma coeficientilor
					{
				       monom.setCoefficient(monom.getCoefficient()+exmonom.getCoefficient());
				       t=1;
					}
			
		}
		//daca in lista nu exista un monom cu aceeasi grad ,adaugam monomul
		if (t==0) monomlyst.add(exmonom);
		return exmonom.getCoefficient()+ "X^"+exmonom.getGrade();
	}
	
	public int getMaxGrade(){
		Monom mon=new Monom();
		for(Monom monom1:monomlyst){
			if (mon.getGrade()<monom1.getGrade()) mon.setGrade(monom1.getGrade());
		}
		return mon.getGrade();
	}
	
	
	
	//scrierea polinomului in final la GUI .metoda returneaza un string ,construit prin parcurgerea listei de monoame
	public String wrpol(){
		String str="";
		Integer t=0;
		for(Iterator it=monomlyst.iterator();it.hasNext();){
			Monom s = (Monom)it.next();
			if (it.hasNext() && s.getCoefficient()!=0  && t==0  ) str=str+s.getCoefficient()+" X"+"^ "+s.getGrade();
			else if ( s.getCoefficient()!=0 && s.getCoefficient()>0 && t==1  ) str=str+"+"+s.getCoefficient()+" X"+"^ "+s.getGrade();
			else if ( s.getCoefficient()!=0 && s.getCoefficient()<0 && t==1  ) str=str+s.getCoefficient()+" X"+"^ "+s.getGrade();
			else if (s.getCoefficient()!=0)  str=str+ s.getCoefficient()+" X"+"^ "+s.getGrade();
			t=1;
		}
		return str;
		
		
	}
	
}
