import java.util.*;
public class Operations {
	public Operations(){
		super();
	}
	//din clasa operations se realizeaza operatiile asupra monoamelor
	public Polynom addPolynoms(Polynom pol1,Polynom pol2){
		Polynom auxpol=new Polynom();
		
		for(Monom monom1:pol1.monomlyst)
		{
			Monom auxmonom=new Monom();
			int t=0;
			for(Monom monom2:pol2.monomlyst)
			{
				if (monom1.getGrade()==monom2.getGrade()) {auxmonom.setCoefficient(monom1.getCoefficient()+monom2.getCoefficient());
				                                           auxmonom.setGrade(monom1.getGrade());
				                                           
				                                           t=1;} 
				
				}
			if (t==0){ auxmonom.setCoefficient(monom1.getCoefficient());
			           auxmonom.setGrade(monom1.getGrade());
			           }
			
			auxpol.addMonom(auxmonom);
		
			}
		
		
		
		
		for(Monom monom2:pol2.monomlyst)
		{
			Monom auxmonom=new Monom();
			int t=0;
			for(Monom monom1:pol1.monomlyst)
			{
				if (monom2.getGrade()==monom1.getGrade()) {t=1;} 
				
				}
			if (t==0){ auxmonom.setCoefficient(monom2.getCoefficient());
			           auxmonom.setGrade(monom2.getGrade());
			           }
			
			auxpol.addMonom(auxmonom);
		
			}
		
		
		return auxpol;
		}
	//adunarea monoamelor se face prin parcurgerea celor doua liste de monoame si contruita a treia lista prin adaugarea coeficientiilor respectivi
	public Polynom subPolynoms(Polynom pol1,Polynom pol2){
		Polynom auxpol=new Polynom();
		
		for(Monom monom1:pol1.monomlyst)
		{Monom auxmonom=new Monom();
			int t=0;
			for(Monom monom2:pol2.monomlyst)
			{
				if (monom1.getGrade()==monom2.getGrade()) {auxmonom.setCoefficient(monom1.getCoefficient()-monom2.getCoefficient());
				                                           auxmonom.setGrade(monom1.getGrade());
				                                           t=1;} 
				}
			if (t==0){ auxmonom.setCoefficient(monom1.getCoefficient());
			           auxmonom.setGrade(monom1.getGrade());}
			auxpol.addMonom(auxmonom);
			}
		
		
		for(Monom monom2:pol2.monomlyst)
		{
			Monom auxmonom=new Monom();
			int t=0;
			for(Monom monom1:pol1.monomlyst)
			{
				if (monom2.getGrade()==monom1.getGrade()) {t=1;} 
				
				}
			if (t==0){ auxmonom.setCoefficient((-1)*monom2.getCoefficient());
			           auxmonom.setGrade(monom2.getGrade());
			           }
			
			auxpol.addMonom(auxmonom);
		
			}
		
		return auxpol;
		}
	//scaderea se face apraoape analoga ca si adunarea
	public Polynom multiplyPolynoms(Polynom pol1,Polynom pol2){
		Polynom auxpol=new Polynom();
		
		for(Monom monom1:pol1.monomlyst)
		{
			
			for(Monom monom2:pol2.monomlyst)
			{
				Monom auxmonom=new Monom();
				auxmonom.setGrade(monom1.getGrade()+monom2.getGrade());
				auxmonom.setCoefficient(monom1.getCoefficient()*monom2.getCoefficient());
				auxpol.addMonom(auxmonom);
				
			}
			}
			
		return auxpol;
		}
	
	//inmultirea se face prin inmultirea parcurgerea celor doua liste si efectua operatiile necesare pentru inmultire
	public Polynom divisionPolynoms(Polynom pol1,Polynom pol2){
		Polynom auxpol=new Polynom();
		Monom auxmonom=new Monom();
		if (pol1.getMaxGrade()<pol2.getMaxGrade()) System.out.println("Error");
		else{
			
			
		}
			
		return auxpol;
		}
	//derivarea se face prin inmultirea coeficientul monomului cu gradul monomului respectiv ,si scadand gradul
	public Polynom derivatePolynom(Polynom pol){
		Polynom auxpol=new Polynom();
	
		for(Iterator it=pol.monomlyst.iterator();it.hasNext();){
			Monom auxmon=new Monom();
			Monom moo=(Monom)it.next();
			auxmon.setCoefficient(moo.getCoefficient()*moo.getGrade());
			auxmon.setGrade((moo.getGrade())-1);
			auxpol.addMonom(auxmon);
			
		}
		return auxpol;
	}
	
	//integrarea este realizata numai pentru numere intregi
	public Polynom integratePolynom(Polynom pol){
		Polynom auxpol=new Polynom();
	
		for(Iterator it=pol.monomlyst.iterator();it.hasNext();){
			Monom auxmon=new Monom();
			Monom moo=(Monom)it.next();
			auxmon.setCoefficient(moo.getCoefficient()/((moo.getGrade())+1));
			auxmon.setGrade(moo.getGrade()+1);
		
			auxpol.addMonom(auxmon);
		}
		return auxpol;
	}
	
	
	
	}


