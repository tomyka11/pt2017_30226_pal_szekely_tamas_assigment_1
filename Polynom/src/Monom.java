//monomul este baza polinomului ,mai bine spus nucleul proiectului
public class Monom {
	private int grade; //el are un grad
	private int coefficient;//un coeficient

	
	public Monom(int grade,int coefficient){//constructorul clasei
		this.grade=grade;
		this.coefficient=coefficient;
	}
	public Monom(){//declaram un constructor fara parametrii
		super();
	}
	//metodele getter si setter,pentru a modifica atributele clasei monom,deoarece aceste sunt declarate private
	public int getCoefficient(){
		return this.coefficient;
	}
	public void setCoefficient(int coefficient){
		this.coefficient=coefficient;
	}
	public int getGrade(){
		return this.grade;
	}
	public void setGrade(int grade){
		this.grade=grade;
	}
	

}
